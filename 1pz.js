/*
Import data seta sa https://archive.ics.uci.edu/dataset/432/news+popularity+in+multiple+social+media+platforms
je napravljen putem konzole koristeći mongoimport aplikaciju:
mongoimport --db baza --collection novosti --type csv --headerline --ignoreBlanks --file C:\NOSQLBDA\News_Final_500.csv

Naredba za učitavanje ove skripte u MongoDB Shellu: 
load("C:/NOSQLBDA/1PZ.js")

##################
#
# OBJASNJENJE ZNACENJA ATRIBUTA IZ DATA SETA
#
# IDLink (numeric): Unique identifier of news items
# Title (string): Title of the news item according to the official media sources
# Headline (string): Headline of the news item according to the official media sources
# Source (string): Original news outlet that published the news item
# Topic (string): Query topic used to obtain the items in the official media sources
# PublishDate (timestamp): Date and time of the news items' publication
# SentimentTitle (numeric): Sentiment score of the text in the news items' title
# SentimentHeadline (numeric): Sentiment score of the text in the news items' headline
# Facebook (numeric): Final value of the news items' popularity according to the social media source Facebook
# GooglePlus (numeric): Final value of the news items' popularity according to the social media source Google+
# LinkedIn (numeric): Final value of the news items' popularity according to the social media source LinkedIn
#
##################
*/

var mongo = new Mongo();
var db = mongo.getDB("baza");

const kategoricki_atributi = ["Title","Headline","Source","Topic","PublishDate"]
const numericki_atributi = ["SentimentTitle","SentimentHeadline","Facebook","GooglePlus","LinkedIn"]

//1. Korak: zamjena nedostajućih vrijednosti - kontinuirane varijable sa -1, a kategoričke sa „empty“
print("1. Korak - zamjena nedostajućih vrijednosti:")

function Zamijeni_vrijednosti(atributi, zamjenska_vrijednost, poruka)
{
    atributi.forEach(function (atribut) {
        rezultat = db.novosti.updateMany(
            { [atribut]: { $exists: false } },
            { $set: { [atribut]: zamjenska_vrijednost } }
        )
        print(poruka)
        print("Atribut: " + atribut)
        print(rezultat)
    });
};


Zamijeni_vrijednosti(kategoricki_atributi, "empty", "Rezultat zamjene nedostajućih kategoričkih vrijednosti")
Zamijeni_vrijednosti(numericki_atributi, -1, "Rezultat zamjene nedostajućih numeričkih vrijednosti")

//2. Korak: Za kontinuirane vrijednosti - izračun srednje vrijednosti, standardne devijacije, broj nomissing elemenata
print("2. Korak - izračun srednje vrijednosti i standardne devijacije te kreiranje dokumenta:")

numericki_atributi.forEach(function (atribut) {
    izracun = db.novosti.aggregate([
        {
            $match: {
                [atribut]: { $exists: true, $ne: null }
            }
        },
        {
            $group: {
                _id: null,
                sv: { $avg: "$" + atribut },
                stdev: { $stdDevSamp: "$" + atribut },
                broj: { $sum: 1 }
            }
        }
    ])
    rezultat = izracun.toArray()[0];

    var noviDokument = {
        varijabla: atribut,
        srednja_vrijednost: rezultat.sv,
        standardna_devijacija: rezultat.stdev,
        broj_nomissing_elemenata: rezultat.broj
    };
    db.statistika_novosti.insertOne(noviDokument)

});

print(db.statistika_novosti.find())

//3. Korak: izračun frekvencije pojavnosti koristeći $inc
print("3. Korak: izračun frekvencije pojavnosti za kategoričke varijable koristeći $inc:")

var kolekcija = db.getCollection("novosti");

kategoricki_atributi.forEach(function (atribut) {
    var cursor = kolekcija.find({})
    cursor.forEach(function(dokument) {
    if (dokument[atribut] != undefined)
    {
        db.frekvencija_novosti.updateOne(
            {"varijabla": atribut},
            //bila je potrebna zamjena specijalnih znakova u stringu
            { $inc: { ["Pojavnost." + dokument[atribut].replace(/[^\w\s]/gi, '').toString()] : 1 } },
            { upsert: true }
        );
    }
    });

});

print(db.frekvencija_novosti.find())

//4. Korak
print("4. Korak")

numericki_atributi.forEach(function (atribut) {
    var naziv_kolekcije1= "statistika1_" + atribut;
    var naziv_kolekcije2= "statistika2_" + atribut;
    srednja_vrijednost = db.statistika_novosti.findOne({varijabla: atribut}).srednja_vrijednost
    print(atribut+ " srednja vrijednost: " + srednja_vrijednost)

    //manji ili jednaki od srednje vrijednosti
    manji_jednaki_sv = db.novosti.find({[atribut]: {$lte: srednja_vrijednost}});
    manji_jednaki_sv.forEach(function(dokument) {
        db[naziv_kolekcije1].insertOne(dokument)
    });

    print(naziv_kolekcije1)
    print(db[naziv_kolekcije1].findOne())

     //veći od srednje vrijednosti
     veci_od_sv = db.novosti.find({[atribut]: {$gt: srednja_vrijednost}});
     veci_od_sv.forEach(function(dokument) {
         db[naziv_kolekcije2].insertOne(dokument)
     });
 
     print(naziv_kolekcije2)
     print(db[naziv_kolekcije2].findOne())
   

});

//5. korak

function kopiraj_i_embedaj(nova_kolekcija, atributi, iz_kolekcije, prefix){
    db.novosti.aggregate([
        { $match: {} },  
        { $out: nova_kolekcija }  
    ])

    atributi.forEach(function (atribut) {
        var za_embedanje = db[iz_kolekcije].find({varijabla: atribut}).toArray()[0]
        var naziv_novog_atributa = prefix + atribut;
        db[nova_kolekcija].updateMany(
            {},
            { $set: { [naziv_novog_atributa]: za_embedanje } },
        )
        print(db[nova_kolekcija].findOne())
    });

};

print("5. Korak: embedanje kategoričkih atributa iz frekvencija_novosti")

kopiraj_i_embedaj("novosti1", kategoricki_atributi, "frekvencija_novosti", "emb_")

//6. korak
print("6. Korak: embedanje numeričkih atributa iz statistika_novosti")

kopiraj_i_embedaj("novosti2", numericki_atributi, "statistika_novosti", "emb2_")

//7. korak
print("7. Korak: Izvlačenje iz tablice emb2 srednjih vrijednosti čija je standardna devijacija 10% > srednje vrijednosti koristeći $set modifikator")

numericki_atributi.forEach(function (atribut) {
    var naziv_atributa = "emb2_" + atribut;
    var novi_atribut = naziv_atributa + ".rezultat";
    var atribut_stdev = naziv_atributa + ".standardna_devijacija";
    var atribut_sv = naziv_atributa + ".srednja_vrijednost";
    db.novosti2.updateMany(
        { $expr: { $gt: ["$"+atribut_stdev, { $multiply: [parseFloat(["$"+atribut_sv]), 1.1] }] } },
        { $set: { [novi_atribut]: "sd>sv*1.1" } },
    );
    print(db.novosti2.find({[novi_atribut]: { $exists: true } }));
});

//8. korak
print("8. Korak: kreiranje složenog indeksa i izvršavanje upita db.novosti.find({ IDLink: { $lte: 25000 }}).explain()")
db.novosti.createIndex({
    IDLink: 1 //indeksiranje prema uzlaznom redosljedu
 });
 
 print(db.novosti.find({ IDLink: { $lte: 25000 }}).explain()); //explain() - izvještaj o izvršenju upita
